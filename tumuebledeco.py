
        # get a session!
#         session = InstaPy(username=insta_username,
#                         password=insta_password,
#                         headless_browser=True,
#                         disable_image_load=True,
#                         multi_logs=True)
# from instapy import InstaPy
from instapy import smart_run
from instapy import set_workspace
import schedule
import time
import random
from instapy import InstaPy

#your login credentials
insta_username = 'tumuebledeco'
insta_password = ''

#referencias
referencias = ['infobae', 'puerto_maderas', 'lanacioncom']

#hashtags
# tags = ['muebles', 'deco', 'decoracion']

#path to your workspace
set_workspace(path=None)

session = InstaPy(username=insta_username,
                password=insta_password,
                headless_browser=True,
                disable_image_load=True,
                bypass_suspicious_attempt=True,
                multi_logs=True)

def job():

    with smart_run(session):
        """ Activity flow """
        # settings
        session.set_relationship_bounds(enabled=True,
                                        max_followers=15000)

        # session.set_dont_include(friends)
        # session.set_dont_like(dont_likes)
        # session.set_ignore_if_contains(ignore_list)
        session.set_skip_users(skip_private=False,
                       private_percentage=100)

        session.set_user_interact(amount=2, randomize=True, percentage=60)
        session.set_do_follow(enabled=True, percentage=40)
        session.set_do_like(enabled=True, percentage=80)

        # activity
        session.like_by_tags(random.sample(tags, 3),
                             amount=random.randint(50, 100), interact=True)

        session.unfollow_users(amount=random.randint(75, 150),
                               InstapyFollowed=(True, "all"), style="FIFO",
                               unfollow_after=90 * 60 * 60, sleep_delay=501)
        # session.join_pods()
job()
schedule.every().day.at("09:00").do(job)

while True:
  schedule.run_pending()
  time.sleep(10)